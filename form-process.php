<?php ob_start();
//start session for access to stored data
session_start();
$page_title = 'Saving you Registration..';
require_once('header.php');

try {
    // store the form inputes in variables
    $city = $_POST['city'];
    $email = $_POST['email'];
    $password = $_POST['password'];
    $confirm = $_POST['confirm'];
    $terms = $_POST['terms'];

    // validate inputs
    $ok = true;

    if (empty($city)){
        echo 'City is required<br />';
        $ok = false;
    }

    if (empty($email)){
        echo 'E-mail is required "johndoe@host.com"<br />';
        $ok = false;
    }

    if (empty($password)){
        echo 'Password is required<br />';
        $ok = false;
    }

    if ($password != $confirm){
        echo 'Passwords must match<br />';
        $ok = false;
    }

    if (empty($terms)){
        echo 'Please read and accept our Terms & Conditions<br />';
        $ok = false;
    }

    // check recaptcha
    // set up a curl request to call the recaptcha api
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POST, true);

    // add the values to our curl request
    $post_data = array();
    $post_data['secret'] = "6LcPmgQTAAAAAD2XQXfomdwCcyxkQ-x7mJLoGQqz";
    $post_data['response'] = $_POST['g-recaptcha-response'];
    curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);

    //execue the curl request and store the result that goodle returns
    $result = curl_exec($ch);
    curl_close($ch);
    $result_array = json_decode($result, true);

    if($result_array['success'] != true){
        echo 'Are you human?';
        $ok = false;
    }

    // save if the form is ok 
    if($ok == true){
        
        // connect to the db
        require('db.php');
        
        // SQL command to save the data
        $sql = "INSERT INTO userx (city, email, password, terms) 
                VALUES (:city, :email, :password, :terms)";
        
        // hash password 
        $hashed_password = hash('sha512', $password);
        
        // create a command object
        $cmd = $conn->prepare($sql);
        
        // put each input value into the proper field
        $cmd -> bindParam(':city', $city, PDO::PARAM_STR);
        $cmd -> bindParam(':email', $email, PDO::PARAM_STR);
        $cmd -> bindParam(':password', $hashed_password, PDO::PARAM_STR, 128);
        $cmd -> bindParam(':terms', $terms, PDO::PARAM_BOOL);
        
        // execute the save
        $cmd->execute();
        
        // disonnect
        $conn = null;
        
        // redirect
        header('location:login.php');
        
    }
}

catch (Exception $e) {
    mail('jack.grunin@gmail.com', 'Registration Error', $e);
    header('location:error.php');
}

?>

<div class="jumbotron">

</div>

<?php require('footer.php'); 
ob_flush();?>