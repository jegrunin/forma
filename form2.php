<?php

$page_title = 'Almost there';

require_once('header.php'); ?>

<div id="main">
    <form  method="post" action="form3.php">
        <div class="flirtform">
            <fieldset class="radios">
                <legend><span class="number">2/3</span> I am seeking:</legend>
                <ul id="chkbx">
                    <li>
                        <label for="opposite" class="">Other sex</label>
                        <input id="opposite" type="radio" name="orientation" value="1" />
                    </li>
                    <li>     
                        <label for="bi" class="">Bi sexual</label>
                        <input id="bi" type="radio" name="orientation" value="2" />
                    </li>
                    <li>     
                        <label for="same" class="">Same sex</label>
                        <input id="same" type="radio" name="orientation" value="3" />
                    </li>
                </ul>
                <ul id="chkbx">
                    <li>
                        <label for="partner" class="">One partner</label>
                        <input id="partner" type="radio" name="count" value="4" />
                    </li>
                    <li>     
                        <label for="partners" class="">One+ partners</label>
                        <input id="partners" type="radio" name="count" value="5" />
                    </li>
                </ul>
                <input type="submit" value="Step 3" />
            </fieldset>
        </div>
    </form>
</div>

<?php require_once('footer.php'); ?>