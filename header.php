<!doctype html>
<html lang="en">
<head>
    <title>flirteXtreme</title>
    <meta content="text/html; charset=utf-8" http-equiv="content-type">
    <meta name="viewport" content="width=device-width; initial-scale=1.0; minimum-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css">
    <!-- font awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="./css/normalize.css" />
    <link rel="stylesheet" href="./css/style.css?v=1" media="screen, projection" />
</head>
<body>

<nav class="navbar navbar-inverse" role="navigation">
    <div class="container-fluid">
        <!----- toggle and brand grouped for optimized mobile display ----->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"
            aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="index.php" title="flirteXtreme" class="navbar-brand">
            <img src="./img/FLIRTX2.png" style="width:110px;"></p></a>
        </div>
        <!----- global nav ----->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <?php 
            
                // session_start();
                if (!empty($_SESSION['user_id'])) {
                    //private setup
                    echo  '<li><a href="logout.php" title="Logout">Logout</a></li>';
                    }
                    
                    //public links
                    else {
                    echo '<li><a href="form1.php" title="Register">Register</a></li>
                    <li><a href="login.php" title="Login">Login</a></li>';
                    }
                ?>
            </ul>
        </div>
        <!----- nav collapse into hamburger ----->
    </div>
    <!----- container-fluid ----->
</nav>

<main>