<?php ob_start();

$page_title = 'Log In';

require_once('header.php'); ?>

<div class="flirtform">
    <form method="post" action="validate.php">
        <fieldset>
            <legend>Enter your credentials</legend>
            <input type="email" name="email" placeholder="Your Email *" />
            <input type="password" name="password" placeholder="Your Password *" />

            <input type="submit" value="Log In" />
        </fieldset>
    </form>
</div>

<?php require_once('footer.php');
ob_flush(); ?>

