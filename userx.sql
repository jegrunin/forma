USE gc200310426;

DROP TABLE IF EXISTS userx;

CREATE TABLE userx (
    user_id         INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    city            VARCHAR(50) NOT NULL,
    email           VARCHAR(100) NOT NULL,
    password        CHAR(128) NOT NULL,
    terms           BIT
);

SELECT * FROM userx;