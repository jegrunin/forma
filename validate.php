<?php ob_start();
include('header.php'); 

$email = $_POST['email'];
$password = hash('sha512', $_POST['password']);

require('db.php');

$sql = "SELECT user_id FROM userx WHERE email = :email AND password = :password";

$cmd = $conn->prepare($sql);
$cmd->bindParam(':email', $email, PDO::PARAM_STR);
$cmd->bindParam(':password', $password, PDO::PARAM_STR);
$cmd->execute();
$userx = $cmd->fetchAll();

$count = $cmd->rowCount();
$conn = null;

if ($count == 0) {
	echo 'Invalid Login';
	//exit();	
}
    else {
        session_start(); // Access the existing user session created on the server
        
        foreach  ($userx as $user) {
            $_SESSION['user_id'] = $user['user_id'];
            header('location: default.php');
        }
    }



include('footer.php');
ob_flush(); ?>
